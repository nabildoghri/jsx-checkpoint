import saikik from './img/saikik.jpg';
import './App.css';
import './style.css'
import video from './dbzvid.mp4'
const mystyle = {
  border:'solid 1px black',
  maxWidth:'100vw'
};
function App() {
  return (
    
    <div className="App">
      <div style={{mystyle}}>
        <h1 className="title red">Nabil DOGHRI</h1>
        <br />
        <p>image in SRC</p>
        <img src={saikik} width={320} alt={"File in SRC"}/>

        <br />
        <p>image in PUBLIC</p>
        <img src={process.env.PUBLIC_URL + '/images/witcher.png'} width={320} alt={"file in PUBLIC"}/> 
      </div>
        <p>video in SRC</p>
        <video width={320} height={240} controls>
          <source src={video} type={"video/mp4"} />
        </video>
    </div>
  );
}

export default App;
